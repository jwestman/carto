namespace Carto {
    /**
     * An ornament to display a scale, showing the relationship between sizes
     * on the map and on the ground.
     */
    public class ScaleOrnament : Ornament {
        /**
         * Locales that should use the customary system by default
         */
        public const string[] CUSTOMARY_LOCALES = {
            "unm_US", "es_US", "es_PR", "en_US", "yi_US"
        };

        /**
         * A system of measurement
         */
        public enum UnitSystem {
            // Only two choices: the US customary system and the right one

            /**
             * Metric units (the scale will use meters and kilometers)
             */
            METRIC,

            /**
             * US units (the scale will use feet and miles)
             */
            CUSTOMARY
        }


        /**
         * Font size for the label.
         */
        private const int FONT_SIZE = 10;

        /**
         * Total line width in pixels.
         */
        private const int LINE_WIDTH = 4;
        /**
         * The width of the line minus the outline.
         */
        private const int INNER_LINE_WIDTH = 2;

        /**
         * Margin between the edges of the ornament and the lines
         */
        private const int MARGIN = 6;


        /**
         * #PangoContext for drawing distance labels
         */
        private Pango.Context context;
        /**
         * #PangoLayout for drawing distance labels
         */
        private Pango.Layout layout;


        /**
         * The #UnitSystem to be used, either metric or customary.
         *
         * The default will be chosen based on the user's locale: #CUSTOMARY
         * if the locale is in #CUSTOMARY_LOCALES and #METRIC otherwise.
         */
        public UnitSystem units;


        /**
         * Creates a new ScaleOrnament.
         */
        public ScaleOrnament() {
            this.width = 100;
            this.height = 25;
            this.position = OrnamentPosition.BOTTOM_LEFT;

            var fontmap = Pango.CairoFontMap.get_default();
            this.context = fontmap.create_context();

            this.layout = new Pango.Layout(context);

            string font = "sans-serif bold %dpx".printf(FONT_SIZE);
            var font_desc = Pango.FontDescription.from_string(font);
            this.layout.set_font_description(font_desc);

            string locale = Environment.get_variable("LC_MEASUREMENT")
                            ?? Intl.get_language_names()[0];

            // strip charset info, we just want the locale
            locale = locale.split(".")[0];

            if (locale in CUSTOMARY_LOCALES) this.units = CUSTOMARY;
            else this.units = METRIC;
        }


        public override void render(Cairo.Context ctx) {
            string label;
            double length = this.get_distance(out label);

            // Lines
            ctx.set_line_cap(Cairo.LineCap.ROUND);

            ctx.set_source_rgb(1, 1, 1);
            ctx.set_line_width(LINE_WIDTH);
            this.render_lines(ctx, length);

            ctx.set_source_rgb(0, 0, 0);
            ctx.set_line_width(INNER_LINE_WIDTH);
            this.render_lines(ctx, length);

            // Label
            ctx.translate(MARGIN, 0);
            this.layout.set_text(label, -1);

            // Drawing the outline before the fill makes it much more legible
            ctx.set_source_rgb(1, 1, 1);
            ctx.set_line_width(LINE_WIDTH - INNER_LINE_WIDTH);
            Pango.cairo_layout_path(ctx, this.layout);
            ctx.stroke();

            ctx.set_source_rgb(0, 0, 0);
            Pango.cairo_layout_path(ctx, this.layout);
            ctx.fill();
        }


        /**
         * Renders just the lines of the scale, using the context's current
         * color and line width settings.
         */
        private void render_lines(Cairo.Context ctx, double length) {
            // Start line
            ctx.move_to(MARGIN, MARGIN + FONT_SIZE);
            ctx.line_to(MARGIN, this.height - MARGIN);
            ctx.stroke();

            // End line
            ctx.move_to(MARGIN + length, MARGIN + FONT_SIZE);
            ctx.line_to(MARGIN + length, this.height - MARGIN);
            ctx.stroke();

            // Main line
            ctx.move_to(MARGIN, this.height - MARGIN);
            ctx.line_to(MARGIN + length, this.height - MARGIN);
            ctx.stroke();
        }

        /**
         * Gets a nice, round distance to display.
         */
        private double get_distance(out string label) {
            double length;

            if (this.units == METRIC) {
                int meters = this.get_distance_meters();
                length = meters / this.meters_per_pixel();

                if (meters >= 1000) {
                    label = (meters / 1000).to_string() + " km";
                } else {
                    label = meters.to_string() + " m";
                }
            } else {
                int feet = this.get_distance_feet();
                length = feet / (this.meters_per_pixel() * Utils.FEET_PER_METER);

                if (feet < 5280) {
                    label = feet.to_string() + " ft";
                } else {
                    int miles = this.get_distance_miles();
                    length = miles / (this.meters_per_pixel() * Utils.MILES_PER_METER);
                    label = miles.to_string() + " mi";
                }
            }

            return length;
        }

        /**
         * Calculates the number of pixels per meter at the current lattitude
         * and zoom.
         */
        private double meters_per_pixel() {
            Camera camera = this.renderer.camera;

            // see https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Resolution_and_Scale
            double zoom_exp = Math.exp2(camera.zoom);
            double cos_lat = Math.cos(Utils.deg2rad(camera.center_lat));
            return Utils.METERS_PER_PIXEL * cos_lat / zoom_exp;
        }
        /**
         * Gets the distance displayed by the scale in meters.
         */
        private int get_distance_meters() {
            // metric is so much simpler!
            int width = this.width - MARGIN * 2;
            double meters = width * this.meters_per_pixel();
            return this.floor10(meters);
        }
        /**
         * Gets the distance displayed by the scale in meters.
         */
        private int get_distance_feet() {
            int width = this.width - MARGIN * 2;
            double feet = width * this.meters_per_pixel() * Utils.FEET_PER_METER;
            return this.floor10(feet);
        }
        /**
         * Gets the distance displayed by the scale in meters.
         */
        private int get_distance_miles() {
            int width = this.width - MARGIN * 2;
            double miles = width * this.meters_per_pixel() * Utils.MILES_PER_METER;
            return this.floor10(miles);
        }
        /**
         * Floors @n to a multiple of a power of 10.
         */
        private int floor10(double n) {
            double log = Math.exp10(Math.floor(Math.log10(n)));
            n = Math.floor(n / log);
            n *= log;
            // for some reason we need to round this, not floor or cast
            // otherwise 10 000 000 becomes 9 999 999 ???
            return (int) Math.round(n);
        }
    }
}
