namespace Carto {
    /**
     * The position of an #Ornament on the rendered map.
     */
    public enum OrnamentPosition {
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT;

        public bool top() {
            return this == TOP_LEFT || this == TOP_RIGHT;
        }
        public bool left() {
            return this == TOP_LEFT || this == BOTTOM_LEFT;
        }
    }


    /**
     * An ornament is a "widget" of sorts that can be drawn onto the map.
     *
     * Ornaments are handled by the #Renderer; they are not GTK widgets (and
     * they are named differently to avoid confusion).
     *
     * To create your own ornament, subclass #Ornament. There are also a couple
     * built-in ornaments: #ScaleOrnament and #AttributionOrnament.
     */
    public abstract class Ornament : Object {
        private Renderer _renderer;
        /**
         * The #Renderer this ornament is rendered onto.
         */
        public Renderer renderer {
            get {
                return this._renderer;
            }
            internal set {
                if (this._renderer != null) {
                    warning("Cannot add a CartoOrnament to a renderer twice!");
                    return;
                }

                this._renderer = value;
                this.added_to_renderer();
            }
        }


        /**
         * The position of this ornament on the rendered map.
         */
        public OrnamentPosition position = TOP_LEFT;

        /**
         * The width of the ornament.
         */
        public int width;
        /**
         * The height of the ornament.
         */
        public int height;

        /**
         * Whether the ornament should be shown.
         *
         * This can be used to hide the built-in scale and attribution
         * ornaments, which cannot be removed entirely.
         */
        public bool shown = true;


        /**
         * Called when the ornament is added to a #Renderer.
         *
         * Will only be called once.
         */
        protected virtual void added_to_renderer() {}

        /**
         * Draws the ornament to the Cairo context.
         *
         * The context is already translated based on the ornament's position,
         * width, and height properties.
         */
        public abstract void render(Cairo.Context ctx);
    }
}
