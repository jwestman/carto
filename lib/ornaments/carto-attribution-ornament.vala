namespace Carto {
    /**
     * An #Ornament that displays the attribution for the map.
     */
    public class AttributionOrnament : Ornament {
        /**
         * Font size of the labels, in pixels. May be different than the actual
         * rendered height, due to the way fonts work.
         */
        public const int FONT_SIZE = 10;

        /**
         * Margin around the text
         */
        public const int MARGIN = 2;


        /**
         * A list of #AttributionLabel items to render.
         */
        private Gee.List<AttributionLabel> labels;

        /**
         * A list of #AttributionLogo items to render.
         */
        private Gee.List<AttributionLogo> logos;

        /**
         * The width of the text part of the label
         */
        private int text_width;
        /**
         * The height of the text part of the label
         */
        private int text_height;
        /**
         * The width of the logos
         */
        private int logo_width;


        /**
         * Creates a new AttributionOrnament.
         *
         * The ornament will automatically display the attribution for a
         * renderer when it is added to one.
         */
        public AttributionOrnament() {
            this.labels = new Gee.ArrayList<AttributionLabel>();
            this.logos = new Gee.ArrayList<AttributionLogo>();

            this.position = OrnamentPosition.BOTTOM_RIGHT;
        }


        protected override void added_to_renderer() {
            this.renderer.notify["attribution"].connect((obj, param) => {
                this.update_labels();
            });
            this.update_labels();
        }

        public override void render(Cairo.Context ctx) {
            bool top = this.position.top();
            bool left = this.position.left();

            ctx.save();
                if (!top) ctx.translate(0, this.height - this.text_height);
                if (!left) ctx.translate(this.width - this.text_width, 0);

                ctx.set_source_rgba(1, 1, 1, 0.4);
                ctx.rectangle(0, 0, this.text_width, this.text_height);
                ctx.fill();

                ctx.set_source_rgb(0, 0, 0);
                ctx.translate(MARGIN, MARGIN);
                foreach (var label in this.labels) {
                    label.render(ctx);
                    ctx.translate(label.width + MARGIN * 2, 0);
                }
            ctx.restore();

            ctx.save();
                if (top) ctx.translate(0, this.text_height);
                if (!left) ctx.translate(this.width - this.logo_width, 0);

                foreach (var logo in this.logos) {
                    logo.render(ctx);
                    ctx.translate(logo.width, 0);
                }
            ctx.restore();
        }


        /**
         * Updates the labels.
         */
        private void update_labels() {
            var labels = new Gee.ArrayList<AttributionLabel>();
            var logos = new Gee.ArrayList<AttributionLogo>();

            int text_width = 0;
            int logo_width = 0;
            int text_height = 0;
            int logo_height = 0;

            foreach (var attribution in this.renderer.attribution) {
                var label = new AttributionLabel(attribution);
                labels.add(label);

                text_width += label.width + MARGIN * 2;
                if (text_height == 0 || text_height < label.height) {
                    text_height = label.height;
                }

                if (attribution.image != null) {
                    var logo = new AttributionLogo(attribution);
                    logo_width += logo.width;
                    if (logo_height == 0 || logo_height < logo.height) {
                        logo_height = logo.height;
                    }
                    logos.add(logo);
                }
            }

            this.labels = labels;
            this.logos = logos;

            text_height += MARGIN * 2;
            this.width = int.max(text_width, logo_width);
            this.height = text_height + logo_height;
            this.text_width = text_width;
            this.text_height = text_height;
            this.logo_width = logo_width;
        }
    }


    /**
     * Represents a label for a single attribution notice.
     */
    private class AttributionLabel : Object {
        /**
         * The width of the label in pixels.
         */
        public int width { get; private set; }
        /**
         * The height of the label in pixels.
         */
        public int height { get; private set; }

        /**
         * The text of the label.
         */
        public string text { get; private set; }

        /**
         * The URL to visit when clicking the label.
         */
        public string url { get; private set; }


        /**
         * The #PangoContext for drawing the label
         */
        private Pango.Context context;
        /**
         * The #PangoLayout for drawing the label
         */
        private Pango.Layout layout;


        /**
         * Creates a new #AttributionLabel from the given #Attribution.
         */
        public AttributionLabel(Attribution attribution) {
            this.text = attribution.text;
            this.url = attribution.url;

            var fontmap = Pango.CairoFontMap.get_default();
            this.context = fontmap.create_context();

            this.layout = new Pango.Layout(context);
            this.layout.set_text(attribution.text, -1);

            string font = "sans-serif %dpx".printf(AttributionOrnament.FONT_SIZE);
            var font_desc = Pango.FontDescription.from_string(font);
            this.layout.set_font_description(font_desc);

            int width, height;
            this.layout.get_pixel_size(out width, out height);
            this.width = width;
            this.height = height;
        }


        /**
         * Renders the label.
         */
        public void render(Cairo.Context ctx) {
            Pango.cairo_show_layout(ctx, this.layout);
        }
    }


    /**
     * Represents a logo in an attribution notice
     */
    private class AttributionLogo : Object {
        /**
         * The logo to be drawn
         */
        private Gdk.Pixbuf logo { get; private set; }

        /**
         * The width of the logo
         */
        public int width { get; private set; }
        /**
         * The height of the logo
         */
        public int height { get; private set; }


        /**
         * Creates a new AttributionLogo for the given attribution.
         *
         * The #Attribution must not have an `image` property of `null`.
         */
        public AttributionLogo(Attribution attribution) {
            this.logo = attribution.image;
            this.width = this.logo.width;
            this.height = this.logo.height;
        }


        /**
         * Draws the logo to the context.
         */
        public void render(Cairo.Context ctx) {
            Gdk.cairo_set_source_pixbuf(ctx, this.logo, 0, 0);
            ctx.paint_with_alpha(0.7);
        }
    }
}
