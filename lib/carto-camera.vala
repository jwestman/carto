namespace Carto {
    using Utils;

    /**
     * The maximum zoom level that is allowed.
     */
    const double MAX_ZOOM = 19;

    /**
     * The pixel size of a standard tile.
     */
    const int TILE_SIZE = 256;


    /**
     * Represents a "camera" for a Renderer, including the location, zoom
     * level, and rotation.
     */
    public class Camera {
        private double _zoom = 0;
        /**
         * The zoom level of this camera.
         * 
         * Zoom is on a base 2 logarithmic scale, where a zoom level of 0 means
         * the entire world is TILE_SIZE pixels across. A zoom increase of 1
         * corresponds to doubling the pixel size.
         */
        public double zoom {
            get {
                return _zoom;
            }
            set {
                _zoom = value.clamp(0, MAX_ZOOM);
            }
        }

        /**
         * The integer part of the zoom level.
         */
        public int zoom_int {
            get {
                return (int) this.zoom;
            }
        }

        /*
         * The X coordinate of the center of the map.
         * 
         * Measured in tile coordinates at zoom level 0 (this means it should
         * always range from 0 to 1).
         */ 
        public double center_x = 0.5;
        /*
         * The Y coordinate of the center of the map.
         * 
         * Measured in tile coordinates at zoom level 0 (this means it should
         * always range from 0 to 1).
         */
        public double center_y = 0.5;

        /**
         * The lattitude of the center of the map.
         */
        public double center_lat {
            get {
                // see https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Tile_numbers_to_lon..2Flat.
                return Utils.rad2deg( Math.atan(Math.sinh(Math.PI * (1 - 2 * center_y))) );
            }
        }
        /**
         * The longitude of the center of the map.
         */
        public double center_lon {
            get {
                return this.center_x * 360 - 180;
            }
        }

        /*
         * The map's rotation in radians.
         * 
         * A value of 0 means that geographic north is in the -Y direction. A
         * value of Math.PI/2 means that geographic north is in the -X
         * direction.
         */
        public double rotation = 0;

        /**
         * The width of the camera viewport, in pixels.
         */
        public int width = 0;
        /**
         * The height of the camera viewport, in pixels.
         */
        public int height = 0;

        /**
         * Hint that map elements should not download data.
         *
         * This is just a hint, and does not need to be followed strictly. It
         * is usually set when the camera is moving quickly, and any downloaded
         * tiles would probably no longer be in view by the time they arrive.
         */
        public bool avoid_io = false;


        /*
         * Creates a new Camera with all values set to 0.
         */
        public Camera() {
        }


        /**
         * Returns the number of tiles in the grid at the current zoom level.
         */
        public int grid_size() {
            return (int) Math.exp2((int) this.zoom);
        }

        /*
         * Gets a bounding box that completely encloses the Camera's view.
         *
         * This is helpful when handling rotated views. The box will be
         * specified in tiles at the zoom level of Math.floor(this.zoom).
         *
         * The box might be larger than the actual world.
         */
        public BoundingBox2D get_bounding_box() {
            // This is surprisingly complicated!

            // Some values we'll be using several times
            double zoom_fraction = Math.exp2(this.zoom % 1);
            double zoom_int = Math.exp2(Math.floor(this.zoom));

            // Get a conversion factor between tiles and pixels
            double pixels_per_tile = TILE_SIZE * zoom_fraction;

            // Get the center of the viewport in tile coords
            Vector2D center = new Vector2D(
                                this.center_x * zoom_int,
                                this.center_y * zoom_int);

            // Find the tile coordinates of each corner of the viewport in
            // tile coords relative to the center
            Vector2D br = new Vector2D(width / 2d, height / 2d)
                                .multiplied_by(1 / pixels_per_tile);
            Vector2D tr = br.flipped_y();
            Vector2D tl = br.flipped_both();
            Vector2D bl = br.flipped_x();

            // Return a bounding box containing all of those vectors
            return new BoundingBox2D.containing_vectors({
                br.rotated(this.rotation).added_to(center),
                tr.rotated(this.rotation).added_to(center),
                tl.rotated(this.rotation).added_to(center),
                bl.rotated(this.rotation).added_to(center)
            }).integer_aligned().expanded_by(1);
        }
    }
}
