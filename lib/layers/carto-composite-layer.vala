namespace Carto {
    /*
     * A layer consisting of multiple sublayers stacked on top of each other.
     * 
     * This can be very useful for organization because it allows you to group
     * layers together. In fact, it is used internally by Carto in several
     * places to split complicated layers into multiple simpler ones.
     */
    public class CompositeLayer : Layer {
        /**
         * The layers that make up this composite layer.
         * 
         * The first layer in the list will be rendered at the bottom.
         */
        private List<Layer> sublayers;


        /*
         * Creates a new CompositeLayer with no sublayers.
         */
        public CompositeLayer() {
            this.sublayers = new List<Layer>();
        }


        /*
         * Adds the given Layer to the end of the list of sublayers.
         * 
         * The layer will then be drawn on top of any previous layers.
         */
        public void add_layer(Layer layer) {
            this.sublayers.append(layer);
            layer.updated.connect(this.on_sublayer_updated);
            layer.notify["attribution"].connect((obj, param) => {
                this.update_attribution();
            });
            this.update_attribution();
        }

        public override void render(Cairo.Context ctx,
                                    Camera camera) {

            foreach (var layer in this.sublayers) {
                layer.render(ctx, camera);
            }
        }


        /**
         * Updates the composite layer's attribution from its sublayers.
         */
        private void update_attribution() {
            var list = new Gee.LinkedList<Attribution>();

            foreach (var layer in this.sublayers) {
                list.add_all(layer.attribution);
            }

            this.attribution = list;
        }


        private void on_sublayer_updated(Layer layer) {
            this.updated();
        }
    }
}
