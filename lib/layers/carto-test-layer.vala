namespace Carto {
    /*
     * A layer containing various debugging information, useful for developing
     * Carto and applications that use it (but not for much else).
     */
    public class TestLayer : Layer {
        /*
         * Creates a new TestLayer with default functionality.
         */
        public TestLayer() {
        }


        public override void render(Cairo.Context ctx, Camera camera) {
            var bb = camera.get_bounding_box();

            double pixel = Utils.pixel(ctx);

            ctx.set_source_rgb(0, 0, 0);
            for(int x = (int) bb.x; x <= bb.x + bb.w; x ++) {
                if (x % 2 == 0) ctx.set_line_width(3 * pixel);
                else ctx.set_line_width(1 * pixel);

                ctx.move_to(x, bb.y);
                ctx.line_to(x, bb.y + bb.h);
                ctx.stroke();
            }

            for(int y = (int) bb.y; y <= bb.y + bb.h; y ++) {
                if (y % 2 == 0) ctx.set_line_width(3 * pixel);
                else ctx.set_line_width(1 * pixel);

                ctx.move_to(bb.x, y);
                ctx.line_to(bb.x + bb.w, y);
                ctx.stroke();
            }
        }
    }
}
