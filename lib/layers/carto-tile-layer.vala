namespace Carto {
    /**
     * A map layer that renders tiles from a TileProvider.
     */
    public class TileLayer : Layer {
        /**
         * The TileProvider that provides tiles to be rendered.
         */
        private TileProvider provider;


        /**
         * Creates a new TileLayer using tiles from the given TileProvider.
         */
        public TileLayer(TileProvider provider) {
            this.provider = provider;

            if (provider.attribution != null) {
                this.attribution.add(provider.attribution);
            }
        }


        public override void render(Cairo.Context ctx, Camera camera) {
            var bb = camera.get_bounding_box();
            int zoom = camera.zoom_int;

            for(int x = (int) bb.x; x <= bb.x + bb.w; x ++) {
                for(int y = (int) bb.y; y <= bb.y + bb.h; y ++) {
                    // the map wraps along the x axis, but not y
                    if (y < 0 || y >= camera.grid_size()) continue;

                    TileLocation location = new TileLocation(zoom, x, y);

                    Tile tile;
                    // every third zoom level, ignore the avoid_io hint
                    if (camera.avoid_io && (zoom % 3 != 0)) {
                        tile = this.provider.query_tile(location);
                    } else {
                        tile = this.provider.get_tile(location);
                    }

                    if (tile != null && tile.state == Tile.ReadyState.READY) {
                        this.render_tile(tile, ctx, camera, x, y, zoom);
                    } else {
                        if (tile != null) {
                            tile.notify["state"].connect(
                                    this.on_tile_state_changed);
                        }

                        while (true) {
                            location = location.get_parent();

                            if (location == null) break;

                            tile = this.provider.query_tile(location);
                            if (tile != null && tile.state == Tile.ReadyState.READY) {
                                this.render_tile(tile, ctx, camera, x, y, zoom);
                                break;
                            }
                        }
                    }
                }
            }
        }


        /**
         * Renders a tile.
         *
         * x, y, and zoom might be different than the tile's actual location.
         * This is so that tiles at lower zoom levels can fill in for tiles at
         * higher levels while they are being loaded.
         */
        private void render_tile(Tile tile, Cairo.Context ctx, Camera camera,
                                 int x, int y, int zoom) {

            double pixel = Utils.pixel(ctx);
            double size_diff = Math.exp2(zoom - tile.location.z);
            int tile_x = (int) Math.floor(x / size_diff);
            int tile_y = (int) Math.floor(y / size_diff);

            ctx.save();

                ctx.scale(size_diff, size_diff);
                ctx.translate(tile_x, tile_y);

                ctx.rectangle(
                    Utils.clock(x / size_diff, 1),
                    Utils.clock(y / size_diff, 1),

                    // Add an extra pixel to prevent tearing
                    (1 / size_diff) + pixel,
                    (1 / size_diff) + pixel
                );
                ctx.clip();

                tile.render(ctx, camera);

            ctx.restore();
        }

        /**
         * Signal handler to update the tile layer when a tile is loaded
         */
        private void on_tile_state_changed(Object obj, ParamSpec param) {
            this.updated();
            obj.notify["state"].disconnect(this.on_tile_state_changed);
        }
    }
}
