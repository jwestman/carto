using Gee;

namespace Carto {
    /**
     * The default max cache size, in bytes.
     *
     * 100 MB.
     */
    public const int DEFAULT_CACHE_SIZE = 100 * 1024 * 1024;

    /**
     * The default max age for cache files, in seconds.
     *
     * 30 days.
     */
    public const int DEFAULT_MAX_AGE = 30 * 24 * 3600;

    /**
     * After how many tile saves to purge the cache.
     */
    public const int PURGE_AFTER = 500;


    /**
     * Provides access to the on-disk tile cache, and manages cache
     * invalidation.
     */
    public class CacheManager : Object {
        private static HashTable<string, CacheManager> instances;
        /**
         * Gets the CacheManager for the given tile source.
         */
        public static CacheManager get_for_tile_source(TileSource source) {
            if (instances == null) {
                instances = new HashTable<string, CacheManager>(str_hash, str_equal);
            }

            CacheManager manager = instances.lookup(source.cache_id);
            if (manager == null) {
                manager = new CacheManager(source);
                instances.insert(source.cache_id, manager);
            }

            return manager;
        }


        /**
         * The ID of this cache.
         */
        public string cache_id { get; private set; }

        /**
         * The approximate maximum size of the cache, in bytes.
         *
         * Note that this value is per cache, and does not apply to all of
         * Carto's caches. It is set by default to %DEFAULT_CACHE_SIZE. Tile
         * sources may recommend a different size.
         */
        public int max_size { get; private set; default=DEFAULT_CACHE_SIZE; }

        /**
         * The maximum length of time that a tile should be cached, in seconds.
         *
         * The default is %DEFAULT_MAX_AGE, but this may be overridden with a
         * value recommended by the tile source.
         */
        public uint64 max_age { get; private set; default=DEFAULT_MAX_AGE; }


        /**
         * The directory for this cache.
         */
        private File cache_dir;

        /**
         * The file extension this cache uses.
         */
        private string file_ext;

        /**
         * How many tile saves left until the cache should be purged.
         *
         * Since this is not persisted between sessions, it starts lower than
         * normal. This way it will probably still be run even if Carto isn't
         * used for very long. However, we don't want it to run immediately,
         * because that could cause a lag spike just as the program is
         * starting.
         */
        private int purge_counter = PURGE_AFTER / 4;


        /**
         * Creates a new CacheManager. Use get_for_cache_id() if you need to
         * obtain a cache manager.
         *
         * The TileSource provides various information about how to configure
         * the cache: which directory and file extensions to use, and possibly
         * how long to keep tiles around.
         */
        private CacheManager(TileSource source) {
            this.cache_id = source.cache_id;
            this.file_ext = source.file_ext;

            string cache_dir = Environment.get_user_cache_dir();
            this.cache_dir = File.new_for_path(cache_dir)
                                    .get_child("carto")
                                    .get_child(this.cache_id);
        }


        /**
         * Gets the cache file for a given tile location, whether that file
         * actually exists or not.
         */
        public File get_cache_file(TileLocation location) {
            return this.cache_dir   .get_child(location.z.to_string())
                                    .get_child(location.x.to_string())
                                    .get_child(location.y.to_string()
                                                + "." + this.file_ext);
        }

        /**
         * Attempts to read the cache file into an InputStream.
         *
         * Returns `null` if the file does not exist or otherwise cannot be
         * read.
         */
        public InputStream? stream_cache_file(TileLocation location) {
            File file = get_cache_file(location);
            try {
                return file.read();
            } catch(Error e) {
                return null;
            }
        }

        /**
         * Asynchronous version of stream_file_cache().
         */
        public async InputStream? stream_cache_file_async(TileLocation location) {
            File file = this.get_cache_file(location);
            try {
                return yield file.read_async();
            } catch(Error e) {
                return null;
            }
        }

        /**
         * Writes the given #InputStream to the cache for the given
         * #TileLocation.
         */
        public async void save_cache_file(TileLocation location, InputStream data) {
            try {
                File file = this.get_cache_file(location);
                yield Utils.mkdir_safe(this.cache_dir.get_parent());
                yield Utils.mkdir_safe(this.cache_dir);
                yield Utils.mkdir_safe(file.get_parent().get_parent());
                yield Utils.mkdir_safe(file.get_parent());

                OutputStream stream = yield file.create_async(
                        FileCreateFlags.REPLACE_DESTINATION);

                yield stream.splice_async(data, OutputStreamSpliceFlags.CLOSE_TARGET);
            } catch (Error e) {
                debug("Error saving cache file: %s", e.message);
            }

            // Purge the cache if needed
            this.purge_counter --;
            if (this.purge_counter <= 0) {
                Idle.add_full(Priority.LOW, this.idle_task_purge);
                this.purge_counter = PURGE_AFTER;
            }
        }


        /**
         * Calls purge_cache(). Can be used as a #SourceFunc.
         */
        private bool idle_task_purge() {
            this.purge_cache.begin();
            return false;
        }
        /**
         * Purges the cache.
         *
         * This operation will attempt to reduce the cache size, if needed, by
         * removing tiles from the cache. Tiles older than the maximum age will
         * be deleted whether the cache is full or not.
         */
        private async void purge_cache() {
            // First, make a list of every file in the cache
            ArrayList<CacheFile?> files = new ArrayList<CacheFile?>();
            yield this.list_files(this.cache_dir, files);
            if (files.size == 0) return;

            /*
             * The algorithm to decide which tiles to delete is as follows:
             * - Find the average tile size
             * - Based on that average, calculate how many tiles will fit in
             * the cache's max size
             * - Using [quickselect](https://en.wikipedia.org/wiki/Quickselect),
             * find the nth oldest tile, where n is the result of the last step
             * - Go through the file list and delete every file older than that
             *
             * This avoids the need to sort the list. It is not perfect, but
             * it's good enough.
             */

            // Average tile size
            uint64 size = 0;
            foreach (var file in files) {
                size += file.size;
            }
            double average = size / (double) files.size;

            // Cache size in tiles
            int max_tiles = (int) (this.max_size / average);

            debug("cache %s: average file size: %f bytes", this.cache_id, average);
            debug("cache %s: tile capacity: %d / %d", this.cache_id, files.size, max_tiles);

            if (max_tiles >= files.size) {
                // cache is already small enough, nothing to do
                return;
            }

            // Find the oldest tile we're going to keep
            CacheFile oldest = this.quickselect(files, max_tiles);

            var date = new GLib.DateTime.from_unix_utc((int64) oldest.access_time);
            debug("cache %s: deleting tiles from before %s", this.cache_id, date.to_string());

            int count = 0;
            foreach (var file in files) {
                if (file.access_time < oldest.access_time) {
                    file.file.delete_async.begin(Priority.LOW);
                    count ++;
                }
            }
            debug("cache %s: deleted %d files", this.cache_id, count);
        }
        /**
         * Lists files for purge_cache().
         */
        private async void list_files(File parent, ArrayList<CacheFile?> list) {
            uint64 earliest = (get_real_time() / 1000000) - this.max_age;

            try {
                FileEnumerator enumerator = yield parent.enumerate_children_async(
                      FileAttribute.STANDARD_NAME + ","
                    + FileAttribute.STANDARD_SIZE + ","
                    + FileAttribute.TIME_ACCESS + ","
                    + FileAttribute.TIME_MODIFIED,
                    FileQueryInfoFlags.NONE
                );

                FileInfo info = null;
                while (true) {
                    info = enumerator.next_file();
                    if (info == null) break;

                    if (info.get_file_type() == FileType.DIRECTORY) {
                        File child = parent.get_child(info.get_name());
                        yield this.list_files(child, list);
                    } else {
                        CacheFile file = CacheFile() {
                            access_time = info.get_attribute_uint64(FileAttribute.TIME_ACCESS),
                            change_time = info.get_attribute_uint64(FileAttribute.TIME_MODIFIED),
                            size = info.get_size(),
                            file = parent.get_child(info.get_name())
                        };

                        if (file.change_time < earliest) {
                            // if the file is older than the max age, just
                            // delete it. don't put it in the list
                            file.file.delete_async.begin(Priority.LOW);
                        } else {
                            list.add(file);
                        }
                    }
                }
            } catch (Error e) {
                // ignore
            }
        }


        /**
         * Finds the @index'th file in @files as if @files were sorted by
         * `access_time`.
         *
         * Will modify the list.
         */
        private CacheFile quickselect(ArrayList<CacheFile?> list,
                                    int index,
                                    int left=0,
                                    int right=0) {

            if (right == 0) right = list.size - 1;

            if (left == right) return list[left];

            // we don't need to be deterministic
            int pivot_index = Random.int_range(left, right);
            pivot_index = partition(list, left, right, pivot_index);

            if (pivot_index == index) {
                // we found it!
                return list[index];
            } else if (pivot_index > index) {
                // recurse with the first half
                return quickselect(list, index, left, pivot_index - 1);
            } else {
                // recurse with the second half
                return quickselect(list, index, pivot_index + 1, right);
            }
        }

        /**
         * Partitions the given list in place using the given pivot.
         */
        private int partition(ArrayList<CacheFile?> list,
                                int left,
                                int right,
                                int pivot_index) {

            uint64 pivot = list[pivot_index].access_time;

            int index = left;
            for (int i = left; i < right; i ++) {
                // note that we are sorting greatest to least (newest to oldest)
                // not the other way around
                if (list[i].access_time > pivot) {
                    swap(list, i, index);
                    index ++;
                }
            }
            swap(list, right, index);
            return index;
        }

        /**
         * Swaps two items in an ArrayList of cache files.
         */
        private void swap(ArrayList<CacheFile?> list, int i1, int i2) {
            var temp = list[i1];
            list[i1] = list[i2];
            list[i2] = temp;
        }

        /**
         * Represents a cache file.
         */
        private struct CacheFile {
            public uint64 access_time;
            public uint64 change_time;
            public File file;
            public int64 size;
        }
    }
}
