namespace Carto {
    /**
     * A test tile, for testing the tile layer system.
     */
    public class TestTile : Tile {
        /**
         * Tile color (red component).
         */
        private double red;
        /**
         * Tile color (red component).
         */
        private double green;
        /**
         * Tile color (red component).
         */
        private double blue;


        /**
         * Creates a new TestTile.
         */
        public TestTile(TileLocation location) {
            base(location);

            this.red = Random.next_double() % 1;
            this.green = Random.next_double() % 1;
            this.blue = Random.next_double() % 1;

            this.state = READY;
        }


        public override void render(Cairo.Context ctx, Camera camera) {
            ctx.set_source_rgb(this.red, this.green, this.blue);
            ctx.paint();
        }
    }

    /**
     * A TileProvider that provides test tiles.
     */
    public class TestTileProvider : TileProvider {
        /**
         * Memory cache for tiles.
         *
         * Not really necessary for such a simple tile provider, but it's a
         * test provider so we might as well test stuff.
         */
        private MemoryTileCache mem_cache;

        /**
         * Creates a new TestTileProvider.
         */
        public TestTileProvider() {
            this.mem_cache = new MemoryTileCache();
        }


        public override Tile get_tile(TileLocation location) {
            Tile tile = this.mem_cache.get_tile(location);
            if (tile == null) {
                tile = new TestTile(location);
                this.mem_cache.add_tile(tile);
            }
            return tile;
        }

        public override Tile? query_tile(TileLocation location) {
            return this.get_tile(location);
        }
    }
}
