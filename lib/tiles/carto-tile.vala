namespace Carto {
    /**
     * Represents the location of a tile.
     *
     * This includes zoom level as well as X and Y coordinates.
     *
     * TileLocation is immutable.
     */
    public class TileLocation {
        /**
         * Zoom level.
         */
        public int z { get; private set; }

        /**
         * X coordinate.
         */
        public int x { get; private set; }

        /**
         * Y coordinate.
         */
        public int y { get; private set; }


        /**
         * Comparator function for TileLocation.
         *
         * This is useful for constructing a hash table for tiles, such as in
         * MemoryTileCache.
         */
        public static bool compare(TileLocation a, TileLocation b) {
            return (a.z == b.z) && (a.x == b.x) && (a.y == b.y);
        }

        /**
         * Hash function for TileLocation.
         *
         * This is useful for constructing a hash table for tiles, such as in
         * MemoryTileCache.
         */
        public static uint hash(TileLocation tile_location) {
            return     (tile_location.z & 0x15 << 28)
                     ^ (tile_location.x & 0x7FFF << 14)
                     ^ (tile_location.y & 0x7FFF);
        }


        /**
         * Creates a new TileLocation.
         *
         * The coordinates will be checked against the zoom level. If a
         * coordinate is out of bounds, it will be changed to be in bounds
         * using clock arithmetic (that is, the coordinates will
         * "wrap around").
         *
         * Note the argument order: it is (z, x, y), NOT (x, y, z).
         */
        public TileLocation(int z, int x, int y) {
            this.z = z;

            int size = (int) Math.exp2(z);
            this.x = (int) Utils.clock(x, size);
            this.y = (int) Utils.clock(y, size);
        }


        /**
         * Gets the TileLocation for the tile containing this one at the next
         * zoom level out.
         *
         * Returns `null` if this tile is at zoom level 0.
         */
        public TileLocation? get_parent() {
            if (this.z == 0) return null;

            return new TileLocation(
                this.z - 1,
                (int) Math.floor(this.x / 2),
                (int) Math.floor(this.y / 2)
            );
        }
    }


    /**
     * Abstract base class for a map tile.
     *
     * There two types of tiles in Carto: raster and vector. Raster tiles are
     * simply stored as pre-rendered images, and they are represented by the
     * RasterTile subclass. Vector tiles are more complex, since they are
     * rendered client-side by Carto. These are represented by VectorTile.
     *
     * You can also create your own Tile subclass if you wish, though it's
     * usually unnecessary.
     */
    public abstract class Tile : Object {
        /**
         * The loading state of a tile.
         */
        public enum ReadyState {
            /**
             * The tile has just been initialized.
             */
            INITIAL,

            /**
             * The tile has begun to be loaded.
             */
            LOADING,

            /**
             * The tile is ready to be rendered.
             */
            READY,

            /**
             * Loading the tile was attempted, but failed.
             */
            FAILED
        }


        /**
         * The location of this tile on the map.
         */
        public TileLocation location { get; private set; }

        /**
         * The state of this Tile.
         *
         * Once this is set to READY, it should not be changed to anything
         * else. Subclasses should set it to READY when they are done loading
         * and/or parsing, or immediately if no parsing or loading is needed.
         */
        public ReadyState state { get; set; default=INITIAL; }


        /**
         * Creates a new Tile with the given location.
         */
        public Tile(TileLocation location) {
            this.location = location;
        }


        /**
         * Renders the tile.
         *
         * The drawing context should be transformed so that the tile's area is
         * a rectangle starting at the origin, with width and height 1.
         */
        public abstract void render(Cairo.Context ctx, Camera camera);
    }
}
