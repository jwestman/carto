namespace Carto {
    /**
     * Abstract base class for tile providers.
     *
     * A tile provider simply loads tiles from somewhere (or multiple
     * somewheres) and provides them to a TileLayer. It should almost certainly
     * have an in-memory cache. This is not provided by default, but can be
     * easily implemented using the MemoryTileCache class.
     */
    public abstract class TileProvider {
        /**
         * The #Attribution required for this provider.
         */
        public Attribution? attribution;


        public TileProvider() {
        }


        /**
         * Gets a tile at the given location.
         *
         * A tile with the given location must be returned. However, it does
         * not have to be ready to render.
         */
        public abstract Tile get_tile(TileLocation location);


        /**
         * Gets the tile at the given location, if one is already available.
         *
         * This function should only return cached tiles, and should not
         * perform I/O. If the tile is not available, it should return null.
         */
        public abstract Tile? query_tile(TileLocation location);
    }
}
