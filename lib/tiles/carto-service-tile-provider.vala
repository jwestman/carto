namespace Carto {
    /**
     * Provides tiles from a #TileSource, which you can get from a #Service.
     */
    public class ServiceTileProvider : TileProvider {
        /**
         * The tile source describing where to get the tiles from
         */
        private TileSource source;

        /**
         * The tile provider's memory cache
         */
        private MemoryTileCache mem_cache;

        /**
         * The #CacheManager for the tile source
         */
        private CacheManager cache;

        /**
         * The Soup session to use when downloading tiles
         */
        private Soup.Session session;


        /**
         * Creates a new ServiceTileProvider that provides tiles from the
         * given source.
         */
        public ServiceTileProvider(TileSource source) {
            this.source = source;
            this.attribution = source.attribution;

            this.mem_cache = new MemoryTileCache();
            this.cache = CacheManager.get_for_tile_source(source);

            this.session = new Soup.Session();
            this.session.max_conns_per_host = source.max_conns;
        }


        public override Tile get_tile(TileLocation location) {
            // try the memory cache first
            Tile tile = this.mem_cache.get_tile(location);
            if (tile != null) return tile;

            // create a new tile, then begin loading it asynchronously
            switch (this.source.format) {
                case RASTER:
                    tile = new RasterTile(location);
                    break;
            }

            this.load_tile.begin(tile);

            // make sure we add the tile to the memory cache
            this.mem_cache.add_tile(tile);

            return tile;
        }

        public override Tile? query_tile(TileLocation location) {
            return this.mem_cache.get_tile(location);
        }


        /**
         * Loads a tile, first searching the on-disk cache, then downloading
         * the data if it's not in the cache.
         */
        private async void load_tile(Tile tile) {
            tile.state = Tile.ReadyState.LOADING;

            InputStream stream = yield this.cache.stream_cache_file_async(tile.location);

            if (stream == null) {
                try {
                    string url = this.source.get_tile_url(tile.location);
                    var message = new Soup.Message("GET", url);
                    InputStream dl = yield this.session.send_async(message);

                    if (message.status_code == 200) {
                        // stream the data to the cache and also return it
                        var tee = yield new Utils.TeeStream(dl);

                        this.cache.save_cache_file.begin(tile.location, tee.stream_a);
                        stream = tee.stream_b;
                    } else {
                        debug("Failed to load tile, got HTTP %d",
                                (int) message.status_code);
                    }
                } catch (Error e) {
                    // nothing to do, the stream is still null
                    debug("Failed to load tile: %s", e.message);
                }
            }

            // If we got a stream somewhere, load the tile from it
            if (stream != null) {
                switch (this.source.format) {
                    case RASTER:
                        yield ((RasterTile) tile).load(stream);
                        break;
                    default:
                        // should never happen, but just in case
                        tile.state = Tile.ReadyState.FAILED;
                        break;
                }
            } else {
                // well, guess we won't be loading this tile
                tile.state = Tile.ReadyState.FAILED;
            }
        }
    }
}
