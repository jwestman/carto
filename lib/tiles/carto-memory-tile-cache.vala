namespace Carto {
    /**
     * An simple, in-memory cache for tiles.
     */
    public class MemoryTileCache {
        private uint _purge_after = 1000;
        /**
         * Tiles may be purged after this many cache accesses.
         *
         * Note that this is different than the maximum number of tiles that
         * may be stored, due to the way the memory cache handles invalidation.
         * The number of tiles in the cache will usually be less (sometimes
         * much less) than this number.
         *
         * The default is 1000.
         */
        public uint purge_after {
            get {
                return this._purge_after;
            }
            set {
                this._purge_after = value;
                this.purge();
            }
        }


        /**
         * A hash table of the tiles in memory.
         */
        private HashTable<TileLocation, TileCacheEntry> tiles;

        /**
         * The number of times a tile has been accessed.
         *
         * This is a uint64 for obvious reasons--it is incremented every time
         * get_tile() succeeds. It is used to invalidate old tiles in a simple
         * way--if its last_access is less than the current value of
         * access_num - purge_after, then it needs to be purged.
         */
        private uint64 access_num = 0;


        /**
         * Creates a new MemoryTileCache with default parameters.
         */
        public MemoryTileCache() {
            this.tiles = new HashTable<TileLocation, TileCacheEntry>(
                TileLocation.hash, TileLocation.compare
            );
        }


        /**
         * Gets a tile from the cache, if it has one at the given location.
         */
        public Tile? get_tile(TileLocation location) {
            TileCacheEntry entry = this.tiles.lookup(location);
            if (entry == null) return null;

            entry.last_access = this.access_num++;
            return entry.tile;
        }

        /**
         * Adds the given tile to the cache.
         *
         * If a tile already exists there, it will be replaced.
         */
        public void add_tile(Tile tile) {
            var entry = new TileCacheEntry(tile, this.access_num++);
            this.tiles.insert(tile.location, entry);
            this.purge();
        }


        /**
         * Purges old items from the cache.
         *
         * This function does not need to be public because it is called
         * automatically when tiles are added.
         */
        private void purge() {
            if (this.access_num < this.purge_after) return;
            uint64 threshold = this.access_num - this.purge_after;

            this.tiles.foreach_remove((key, val) => {
                return val.last_access < threshold;
            });
        }
    }


    /**
     * An entry in the tile cache.
     *
     * Holds both the tile and its last access time.
     */
    private class TileCacheEntry {
        public Tile tile { get; private set; }
        public uint64 last_access;

        public TileCacheEntry(Tile tile, uint64 last_access) {
            this.tile = tile;
            this.last_access = last_access;
        }
    }
}
