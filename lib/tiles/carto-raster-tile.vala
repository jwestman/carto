namespace Carto {
    /**
     * A tile that is drawn from a pre-rendered image.
     */
    public class RasterTile : Tile {
        /**
         * The pixbuf to be drawn for this tile
         */
        private Gdk.Pixbuf pixbuf;


        public RasterTile(TileLocation location) {
            base(location);
        }


        /**
         * Loads the tile from an #InputStream containing image data.
         */
        public async void load(InputStream stream) {
            try {
                this.pixbuf = yield new Gdk.Pixbuf.from_stream_async(stream);

                this.state = ReadyState.READY;
            } catch (Error e) {
                this.state = ReadyState.FAILED;
            }
        }


        public override void render(Cairo.Context ctx, Camera camera) {
            ctx.scale(1d / this.pixbuf.width, 1d / this.pixbuf.height);
            Gdk.cairo_set_source_pixbuf(ctx, this.pixbuf, 0, 0);

            // Pad the edges of the tile to cover up tearing artifacts
            ctx.get_source().set_extend(Cairo.Extend.PAD);
            ctx.paint();
        }
    }
}
