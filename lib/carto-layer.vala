namespace Carto {
    /*
     * Abstract base class for map layers.
     */
    public abstract class Layer : Object {
        /*
         * Should be emitted by the Layer when something changes that requires
         * (or may require) that the map be rendered again. 
         */
        public signal void updated();


        /**
         * A list of #Attribution objects describing any necessary attribution
         * for the data in this layer.
         */
        public Gee.List<Attribution> attribution { get; protected set; }


        public Layer() {
            this.attribution = new Gee.LinkedList<Attribution>();
        }


        /**
         * Renders the layer to the given Cairo.Context.
         * 
         * The given context is already transformed according to the current
         * view. One standard tile at the current zoom level is one unit in the
         * transformed context. That is, to render tile (3, 4) at the current
         * zoom, you should use `ctx.rectangle(3, 4, 1, 1)`.
         * 
         * Several useful functions are available in the Camera object, most
         * notably Camera.get_bounding_box().
         */
        public abstract void render(Cairo.Context ctx, Camera camera);
    }
}
