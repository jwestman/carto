/**
 * Basic utility functions that make sense to include in a cartography library.
 */
namespace Carto.Utils {
    /**
     * The circumference of Earth in meters
     *
     * see https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Resolution_and_Scale
     */
    public const double EQUATOR_CIRCUMFERENCE = 40075016.686;

    /**
     * Number of meters per pixel at the equator
     */
    public const double METERS_PER_PIXEL = EQUATOR_CIRCUMFERENCE / TILE_SIZE;

    /**
     * Number of feet in a meter
     */
    public const double FEET_PER_METER = 3.28084;

    /**
     * Number of miles in a meter
     */
    public const double MILES_PER_METER = FEET_PER_METER / 5280;


    /**
     * Returns d degrees in radians because you're too lazy to remember
     * (Math.PI * d) / 180.
     */
    public double deg2rad(double d) {
        return (Math.PI * d) / 180;
    }

    /**
     * Converts r radians to degrees.
     */
    public double rad2deg(double r) {
        return (180 * r) / Math.PI;
    }

    /**
     * A simple clock arithmetic function.
     *
     * This differs from Math.fmod() and the % operator in its handling of
     * negative numbers. The result will always be in the range 0-divisor, even
     * if the dividend is negative.
     */
    public double clock(double dividend, double divisor) {
        double quotient = Math.floor(dividend / divisor);
        return dividend - (quotient * divisor);
    }

    /**
     * Gets the size, in user coordinates, of a pixel on the screen, given the
     * transformations on the given context.
     */
    public double pixel(Cairo.Context ctx) {
        double x = 0, y = 1;
        ctx.device_to_user_distance(ref x, ref y);
        return Math.hypot(x, y);
    }


    /**
     * A generic, immutable BoundingBox2D implementation.
     */
    public class BoundingBox2D {
        public double x { get; private set; default=0; }
        public double y { get; private set; default=0; }
        public double w { get; private set; default=0; }
        public double h { get; private set; default=0; }


        /**
         * Creates a new BoundingBox2D.
         */
        public BoundingBox2D(double x, double y, double w, double h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        }

        /**
         * Creates a new BoundingBox2D from the given coordinates.
         * 
         * The minimum values do not need to come first.
         */
        public BoundingBox2D.from_absolute_coords(double x1,
                                                    double y1,
                                                    double x2,
                                                    double y2) {

            this.x = double.min(x1, x2);
            this.y = double.min(y1, y2);
            this.w = (x1 - x2).abs();
            this.h = (y1 - y2).abs();
        }

        /**
         * Creates a new BoundingBox2D exactly containing all of the given
         * vectors.
         * 
         * If no vectors are supplied, then the bounding box will have all
         * zeros as values.
         */
        public BoundingBox2D.containing_vectors(Vector2D[] vectors) {
            if (vectors.length == 0) return;

            double min_x = vectors[0].x;
            double max_x = vectors[0].x;
            double min_y = vectors[0].y;
            double max_y = vectors[0].y;
            foreach (var vector in vectors) {
                min_x = double.min(min_x, vector.x);
                max_x = double.max(max_x, vector.x);
                min_y = double.min(min_y, vector.y);
                max_y = double.max(max_y, vector.y);
            }

            this.from_absolute_coords(min_x, min_y, max_x, max_y);
        }


        /**
         * Returns the smallest BoundingBox2D that fits around this one while
         * having only integer coordinates and dimensions.
         */
        public BoundingBox2D integer_aligned() {
            return new BoundingBox2D(
                (int) Math.floor(this.x),
                (int) Math.floor(this.y),
                (int) Math.ceil(this.w + (this.x % 1).abs()),
                (int) Math.ceil(this.h + (this.y % 1).abs())
            );
        }
        
        /**
         * Returns a new BoundingBox2D representing this one expanded by amount
         * in every direction.
         */
        public BoundingBox2D expanded_by(double amount) {
            return new BoundingBox2D(
                this.x - amount,
                this.y - amount,
                this.w + (amount * 2),
                this.h + (amount * 2)
            );
        } 
    }


    /**
     * A generic, immutable Vector2D implementation.
     */
    public class Vector2D {
        public double x { get; private set; }
        public double y { get; private set; }


        /**
         * Creates a new Vector2D.
         */
        public Vector2D(double x, double y) {
            this.x = x;
            this.y = y;
        }


        /**
         * Returns a new Vector2D representing this Vector2D rotated
         * counterclockwise by r radians.
         */
        public Vector2D rotated(double r) {
            return new Vector2D(this.x * Math.cos(-r) - this.y * Math.sin(-r),
                                this.x * Math.sin(-r) + this.y * Math.cos(-r));
        }

        /**
         * Returns a new Vector2D representing this Vector2D multiplied by a
         * scalar.
         */
        public Vector2D multiplied_by(double scalar) {
            return new Vector2D(this.x * scalar, this.y * scalar);
        }

        /**
         * Returns a new Vector2D representing this Vector2D added to another.
         */
        public Vector2D added_to(Vector2D other) {
            return new Vector2D(this.x + other.x, this.y + other.y);
        }

        /**
         * Returns a new Vector2D representing this Vector2D flipped over the X
         * axis.
         */
        public Vector2D flipped_x() {
            return new Vector2D(this.x, -this.y);
        }

        /**
         * Returns a new Vector2D representing this Vector2D flipped over the Y
         * axis.
         */
        public Vector2D flipped_y() {
            return new Vector2D(-this.x, this.y);
        }

        /**
         * Returns a new Vector2D representing this Vector2D flipped over both
         * axes.
         * 
         * Returns the same result as rotated(Math.PI).
         */
        public Vector2D flipped_both() {
            return new Vector2D(-this.x, -this.y);
        }
    }


    /* Private internal utilities */

    /**
     * Backport of Json.Object.get_string_member_with_default(), which is in
     * the currently unreleased 1.6 version of json-glib.
     */
    internal string json_get_string(Json.Object obj,
                                    string key,
                                    string? default_val=null) {

        Json.Node member = obj.get_member(key);
        if (member == null) return default_val;

        string val = member.get_string();
        if (val == null) return default_val;

        return val;
    }

    /**
     * mkdir that doesn't fail if the directory exists.
     */
    internal async void mkdir_safe(File dir) {
        try {
            yield dir.make_directory_async();
        } catch (Error e) {

        }
    }


    /**
     * Splits an input stream in two.
     *
     * The current implementation reads the original stream in its entirety,
     * then creates two new MemoryInputStreams with that data.
     */
    internal class TeeStream : Object {
        /**
         * Chunk size to read the original buffer
         */
        private const int CHUNK_SIZE = 4096;

        /**
         * The stream to read from
         */
        private InputStream input;


        /**
         * One of the resulting streams
         */
        public MemoryInputStream stream_a { get; private set; }
        /**
         * The other resulting stream
         */
        public MemoryInputStream stream_b { get; private set; }


        /**
         * Creates a new TeeStream containing two streams, each containing the
         * data of the given stream.
         */
        public async TeeStream(InputStream input) throws Error {
            this.input = input;

            this.stream_a = new MemoryInputStream();
            this.stream_b = new MemoryInputStream();

            while (true) {
                Bytes bytes = yield this.input.read_bytes_async(CHUNK_SIZE);
                if (bytes.length == 0) {
                    break;
                }
                this.stream_a.add_bytes(bytes);
                this.stream_b.add_bytes(bytes);
            }
        }
    }
}
