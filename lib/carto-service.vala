namespace Carto {
    /**
     * Tile data format.
     *
     * No distinction is made between different raster formats (PNG, JPEG,
     * etc.) because that is detected by GdkPixbuf and we don't need to worry
     * about it.
     */
    public enum TileFormat {
        RASTER,
        VECTOR
    }

    /**
     * Represents a set of map services found in a service.json file.
     *
     * Applications that use Carto can load their own services if they want to
     * use their own services without implementing a custom TileProvider.
     */
    public class Service : Object {
        /**
         * A URL to download the service.json file.
         *
         * This allows details like API keys to be changed remotely, without
         * an update to Carto or any application that uses it.
         */
        public const string SERVICE_API
                = "https://gis.gnome.org/services/v1/service.json";

        /**
         * The path of the GResource for the service file.
         */
        public const string SERVICE_RESOURCE
                = "/net/flyingpimonster/Carto/service.json";


        private static Service instance;
        /**
         * Gets the standard instance of Service that uses the built-in
         * service.json file.
         *
         * The search path for this file is as follows:
         * - The environment variable MAPS_SERVICE
         * - At the URL %SERVICE_API
         * - In Carto's built-in data files (see data/service.json in the
         * source repository)
         */
        public static Service get_standard() {
            if (instance == null) {
                string bytes = try_load_from_env_var()
                                ?? try_load_from_api()
                                ?? load_from_resource();

                try {
                    instance = new Service.from_json(bytes);
                } catch (Error e) {
                    warning("Error finding or parsing service.json: %s", e.message);
                    instance = new Service();
                }
            }
            return instance;
        }


        /**
         * A map of the tile sources by their IDs.
         */
        private HashTable<string, TileSource> sources;


        /**
         * Creates a new Service object with no tile sources.
         */
        public Service() {
        }

        /**
         * Creates a new Service object from the given service.json data.
         */
        public Service.from_json(string json) throws Error {
            /* This code is intended to be backward compatible with the format
             * that GNOME Maps uses for its maps-service.json file. As such, if
             * any information is missing from the file, we'll just assume it's
             * a raster source. */

            this.sources = new HashTable<string, TileSource>(str_hash, str_equal);

            var parser = new Json.Parser();
            parser.load_from_data(json);

            var root = parser.get_root().get_object();
            var tiles = root.get_object_member("tiles");
            var tile_source_ids = tiles.get_members();

            foreach (string id in tile_source_ids) {
                var obj = tiles.get_object_member(id);

                var source = new TileSource(id);

                /* For backward compatibility, the tile source's ID is the key
                 * in the JSON, and the cache_id is stored in the "id"
                 * member. */
                source.cache_id = Utils.json_get_string(obj, "id", id);

                string format = Utils.json_get_string(obj, "format", "raster");
                if (format == "vector") source.format = TileFormat.VECTOR;
                else source.format = TileFormat.RASTER;

                source.file_ext = Utils.json_get_string(obj, "file_ext", "jpg");
                source.url_template = Utils.json_get_string(obj, "uri_format");
                source.max_conns = (int) obj.get_int_member("max_connections");

                string attr_text = Utils.json_get_string(obj, "license");
                string attr_url = Utils.json_get_string(obj, "license_uri");
                string attr_img_enc = Utils.json_get_string(obj, "attribution_logo");
                Gdk.Pixbuf attr_image = null;
                if (attr_img_enc != null) {
                    uchar[] bytes = Base64.decode(attr_img_enc);
                    var stream = new MemoryInputStream.from_data(bytes);
                    attr_image = new Gdk.Pixbuf.from_stream(stream);
                }
                source.attribution = new Attribution(attr_text, attr_url, attr_image);

                this.sources.insert(id, source);
            }
        }


        /**
         * Gets a TileSource from the service by ID.
         */
        public TileSource? get_tile_source(string id) {
            return this.sources.lookup(id);
        }
    }


    /**
     * Represents a single tile source from a Service.
     */
    public class TileSource : Object {
        /**
         * The ID of this tile source.
         *
         * Probably either "street" or "aerial", but other values are allowed.
         */
        public string id { get; private set; }

        /**
         * The cache directory for this tile source.
         *
         * This is distinct from the ID (which would usually be "street" or
         * "aerial"). It should be unique to the tile source. An example would
         * be "mapbox.streets-v4".
         */
        public string cache_id;

        /**
         * The filename extension for the tile source.
         */
        public string file_ext;

        /**
         * The TileFormat of the tiles from this source.
         */
        public TileFormat format;

        /**
         * The template for the URL to download tiles from.
         *
         * Zoom, X coordinate, and Y coordinate should be replaced with `#Z#`,
         * `#X#`, and `#Y#` respectively.
         */
        public string url_template;

        /**
         * Maximum number of concurrent connections that should be made to the
         * host.
         */
        public int max_conns = 2;

        /**
         * The Attribution for this source.
         */
        public Attribution? attribution;


        /**
         * Creates a new TileSource with the given ID.
         */
        public TileSource(string id) {
            this.id = id;
        }


        /**
         * Gets the URL for a tile location using this tile source's URL
         * template.
         */
        public string get_tile_url(TileLocation location) {
            return this.url_template.replace("#Z#", location.z.to_string())
                                    .replace("#X#", location.x.to_string())
                                    .replace("#Y#", location.y.to_string());
        }
    }


    /**
     * Tries loading the service.json file from the MAPS_SERVICE
     * environment variable.
     */
    private static string? try_load_from_env_var() {
        string maps_service = Environment.get_variable("MAPS_SERVICE");
        if (maps_service != null) {
            var file = File.new_for_path(maps_service);
            try {
                uint8[] bytes;
                bool success = file.load_contents(null, out bytes, null);

                if (success) return (string) bytes;
            } catch (Error e) {
                // warn the user, but otherwise move on
                warning("Error loading file `%s', from MAPS_SERVICE environment variable: %s",
                        file.get_path(), e.message);
            }
        }

        // we failed
        return null;
    }

    /**
     * Tries loading the service.json file from the %SERVICE_API url.
     */
    private static string? try_load_from_api() {
        var session = new Soup.Session();
        var message = new Soup.Message("GET", Service.SERVICE_API);
        session.send_message(message);

        if (message.status_code == 200) {
            return (string) message.response_body.flatten().data;
        } else {
            return null;
        }
    }

    /**
     * Loads the service.json file from Carto's built-in resource files.
     */
    private static string? load_from_resource() {
        try {
            Bytes bytes = resources_lookup_data(Service.SERVICE_RESOURCE,
                                                ResourceLookupFlags.NONE);

            return (string) Bytes.unref_to_data(bytes);
        } catch (Error e) {
            // that shouldn't have happened...
            warning("Error loading service.json GResource: %s", e.message);
            return null;
        }
    }
}
