namespace Carto {
    /*
     * A widget to display a map.
     */
    public class View : Gtk.Overlay {
        /*
         * The Renderer that this View uses to render its map.
         */
        private Renderer renderer;

        /*
         * The Gtk.DrawingArea that the map is drawn onto.
         */ 
        private Gtk.DrawingArea map_surface;
        
        /**
         * Used to determine delta when rotating via gesture.
         */
        private double last_angle_delta;

        /**
         * Used to determine delta when dragging.
         */
        private double drag_last_x;
        /**
         * Used to determine delta when dragging.
         */
        private double drag_last_y;

        /**
         * An ornament used to display attribution information.
         *
         * If you hide this ornament, you must make sure any necessary
         * attribution is displayed in an appropriate place in your app. You
         * can get this raw attribution data using the :attribution property.
         */
        public AttributionOrnament attribution_ornament { get; private set; }
        /**
         * An ornament showing the scale of the map.
         */
        public ScaleOrnament scale_ornament { get; private set; }

        /**
         * A list of #Attribution objects for the map layers.
         */
        public Gee.List<Attribution> attribution { get; private set; }


        /*
         * Creates a new CartoView with default parameters.
         */
        public View() {
            this.map_surface = new Gtk.DrawingArea();

            // Events
            var scroll_event = new Gtk.EventControllerScroll(
                Gtk.EventControllerScrollFlags.VERTICAL
            );
            scroll_event.scroll.connect(this.on_scroll);
            this.map_surface.add_controller(scroll_event);

            var drag_event = new Gtk.GestureDrag();
            drag_event.drag_begin.connect(this.on_drag_begin);
            drag_event.drag_update.connect(this.on_drag_update);
            this.map_surface.add_controller(drag_event);

            var zoom_event = new Gtk.GestureZoom();
            zoom_event.scale_changed.connect(this.on_zoom);
            this.map_surface.add_controller(zoom_event);

            var rotate_event = new Gtk.GestureRotate();
            rotate_event.angle_changed.connect(this.on_rotate);
            rotate_event.begin.connect(this.on_rotate_begin);
            this.map_surface.add_controller(rotate_event);

            this.add(this.map_surface);

            // Create the renderer after creating the map surface but before
            // setting its draw function
            this.renderer = new Renderer();
            this.renderer.updated.connect(this.on_renderer_updated);
            this.renderer.notify["attribution"].connect((obj, param) => {
                this.attribution = this.renderer.attribution;
            });

            this.attribution_ornament = new AttributionOrnament();
            this.renderer.add_ornament(this.attribution_ornament);
            this.scale_ornament = new ScaleOrnament();
            this.renderer.add_ornament(this.scale_ornament);

            this.map_surface.set_draw_func(this.map_surface_drawing_func);
        }


        /*
         * Adds a given Layer to the map. The layer will be added on top of any
         * existing layers.
         */
        public void add_layer(Layer layer) {
            this.renderer.add_layer(layer);
        }


        private bool on_scroll(Gtk.EventControllerScroll event,
                                double dx,
                                double dy) {

            this.renderer.zoom(-dy / 5);
            this.renderer.set_avoid_io();
            return false;
        }

        private void on_rotate_begin(Gtk.Gesture gesture,
                Gdk.EventSequence? seq) {

            this.last_angle_delta = 0;
        }

        private void on_rotate(Gtk.GestureRotate gesture,
                double angle,
                double angle_delta) {

            this.renderer.rotate(angle_delta - this.last_angle_delta);
            this.last_angle_delta = angle_delta;
        }

        private void on_drag_update(Gtk.GestureDrag gesture,
                                    double x,
                                    double y) {

            this.renderer.pan(this.drag_last_x - x, this.drag_last_y - y);
            this.drag_last_x = x;
            this.drag_last_y = y;
        }

        private void on_drag_begin(Gtk.GestureDrag gesture,
                                    double x,
                                    double y) {

            this.drag_last_x = 0;
            this.drag_last_y = 0;
        }

        private void on_zoom(Gtk.GestureZoom event, double scale) {
            this.renderer.zoom(Math.log2(scale) / 10);
        }

        private void on_renderer_updated() {
            this.map_surface.queue_draw();
        }

        private void map_surface_drawing_func(Gtk.DrawingArea area,
                                                Cairo.Context ctx,
                                                int width,
                                                int height) {

            this.renderer.render(ctx, width, height);
        }
    }
}
