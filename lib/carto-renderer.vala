namespace Carto {
    /*
     * Renders a map. Usually, you won't need to use this class directly unless
     * you need to render a map without displaying it.
     */
    public class Renderer : Object {
        /*
         * Emitted whenever the output of the renderer might have changed. This
         * includes a number of circumstances, including (but not limited to)
         * when you change the renderer's camera, when an animation is going
         * on, or when a layer signals that it has been updated. 
         */
        public signal void updated();

        /*
         * Emitted when all layers have finished loading their data. This is
         * helpful when rendering maps to targets other than a GUI, to ensure
         * that the output is complete.
         */
        public signal void loading_complete();


        /*
         * A CompositeLayer containing all of the layers that are to be
         * rendered.
         */
        private CompositeLayer layers;

        /*
         * The renderer's Camera, which specifies what area of the map is being
         * rendered.
         */
        public Camera camera;

        /**
         * When to unset the avoid_io property of the camera.
         */
        private int64 avoid_io_until = -1;

        /**
         * A list of #Ornament items to draw over the map.
         */
        private Gee.List<Ornament> ornaments;


        /**
         * A list of #Attribution objects for the layers in the renderer.
         */
        public Gee.List<Attribution> attribution { get; private set; }


        /*
         * Creates a new Renderer with no layers.
         */
        public Renderer() {
            this.attribution = new Gee.LinkedList<Attribution>();

            this.layers = new CompositeLayer();
            this.layers.updated.connect(this.on_layer_updated);
            this.layers.notify["attribution"].connect((obj, param) => {
                this.attribution = this.layers.attribution;
            });

            this.ornaments = new Gee.LinkedList<Ornament>();

            this.camera = new Camera();
        }


        /**
         * Zooms the view by the given amount.
         */
        public void zoom(double amount) {
            this.camera.zoom += amount;
            this.updated();
        }
        /**
         * Rotates the view by the given radians.
         */
        public void rotate(double radians) {
            this.camera.rotation += radians;
            this.updated();
        }
        /**
         * Moves the viewport by a given number of pixels.
         * 
         * Takes rotation and zoom into account.
         */
        public void pan(double x, double y) {
            var vec = new Utils.Vector2D(x, y).rotated(this.camera.rotation);
            double scale_factor = TILE_SIZE * Math.exp2(this.camera.zoom);
            this.camera.center_x += vec.x / scale_factor;
            this.camera.center_y += vec.y / scale_factor;
            this.updated();
        }

        /**
         * Sets the avoid-io property of the renderer's camera.
         *
         * Usually used when navigating the map, to reduce the number of tiles
         * that are downloaded.
         */
        public void set_avoid_io(int milliseconds=500) {
            this.avoid_io_until = get_monotonic_time() + (milliseconds * 1000);
            Timeout.add(milliseconds, this.avoid_io_end);
        }
        /**
         * Called when the avoid IO timeout is reached, to update the renderer
         * if necessary
         */
        private bool avoid_io_end() {
            // make sure there's not another avoid_io set later
            if (this.avoid_io_until <= get_monotonic_time()) {
                this.updated();
            }

            return false;
        }

        /*
         * Adds the given layer to the top of the list of layers that are to be
         * rendered.
         */
        public void add_layer(Layer layer) {
            this.layers.add_layer(layer);
        }

        /**
         * Adds an #Ornament to the map.
         */
        public void add_ornament(Ornament ornament) {
            ornament.renderer = this;
            this.ornaments.add(ornament);
        }

        /*
         * Renders the map to the given Cairo context.
         */
        public void render(Cairo.Context ctx, int width, int height) {
            this.camera.width = width;
            this.camera.height = height;

            this.camera.avoid_io = (this.avoid_io_until > get_monotonic_time());

            ctx.save();

                ctx.translate(width / 2d, height / 2d);
                ctx.rotate(this.camera.rotation);

                int tile_size = (int) (TILE_SIZE
                                        * Math.exp2(this.camera.zoom % 1));
                ctx.scale(tile_size, tile_size);

                double scale_factor_whole =
                        Math.exp2(Math.floor(this.camera.zoom));
                ctx.translate(  -this.camera.center_x * scale_factor_whole,
                                -this.camera.center_y * scale_factor_whole);

                this.layers.render(ctx, this.camera);

            ctx.restore();

            foreach (var ornament in this.ornaments) {
                if (!ornament.shown) continue;

                ctx.save();
                    if (!ornament.position.top()) ctx.translate(0, height - ornament.height);
                    if (!ornament.position.left()) ctx.translate(width - ornament.width, 0);

                    ornament.render(ctx);
                ctx.restore();
            }
        }


        private void on_layer_updated(Layer layer) {
            this.updated();
        }
    }
}
