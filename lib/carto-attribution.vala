namespace Carto {
    /**
     * Provides attribution for a map layer.
     */
    public class Attribution : Object {
        /**
         * The text to display to attribute the source.
         */
        public string? text { get; private set; }

        /**
         * A URL users can visit to find out more about the source.
         */
        public string? url { get; private set; }

        /**
         * An image attached to the attribution, probably a logo.
         */
        public Gdk.Pixbuf? image { get; private set; }


        /**
         * Creates a new #Attribution object with the given information
         *
         * All arguments are optional.
         */
        public Attribution(string? text, string? url, Gdk.Pixbuf? image) {
            this.image = image;
            this.url = url;
            this.text = text;
        }
    }
}
