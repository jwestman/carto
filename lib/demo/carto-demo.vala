namespace Carto {
    int main(string[] args) {
        Gtk.init();

        var window = new DemoWindow();
        window.show(); 

        Gtk.main();

        return 0;
    }

    /*
     * A window for demoing the capabilities of the CartoView widget.
     */
    class DemoWindow : Gtk.Window {
        /**
         * The Carto.View that we will be using to display the demo.
         */ 
        public View map_view;


        /**
         * Creates a new DemoWindow.
         */
        public DemoWindow() {
            // Create the map view and add it to the window
            this.map_view = new View();
            this.add(this.map_view);

            /* We need to add a layer to the map, otherwise it will be blank
             * (which would be boring). For now, we'll create an aerial tile
             * provider. In the future, this will be simplified so that only
             * advanced users need to deal with individual tile providers. */
            var service = Service.get_standard();
            var source = service.get_tile_source("aerial");
            var tile_provider = new ServiceTileProvider(source);
            this.map_view.add_layer(new TileLayer(tile_provider));

            // Quit the application when the window is closed
            this.destroy.connect(this.on_destroy);

            this.set_default_size(512, 512);
        }


        private void on_destroy(Gtk.Widget widget) {
            Gtk.main_quit();
        }
    }
}
